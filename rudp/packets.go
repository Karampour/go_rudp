package rudp

type internalPacket struct {
	Type               string
	ShouldRecvACK      bool
	SequenceNumber     int
	LastRequiredSeqNum int
	Payload            string
}

type ACK struct {
	SequenceNumber int
}
