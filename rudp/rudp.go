package rudp

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"sync"
	"time"
)

type Socket struct {
	Addr       *net.UDPAddr
	RemoteAddr *net.UDPAddr
	conn       *net.UDPConn
	Recv       chan *ExternalClientMessage
	send       chan *internalClientMessage
	close      chan bool
	backLog    []*internalClientMessage
	toBeAck    struct {
		mu   sync.Mutex
		dict map[int]*toBeAcknowledged
	}
	recvQ struct {
		mu   sync.Mutex
		data []*internalClientMessage
	}

	lastACKpacket      int
	lastRecvPacket     int
	nextSequenceNum    int
	lastRequiredSeqNum int
}

type internalClientMessage struct {
	Client *net.UDPAddr
	Packet *internalPacket
}

type ExternalClientMessage struct {
	Addr *net.UDPAddr
	Data []byte
}

type toBeAcknowledged struct {
	Deadline      time.Time
	ClientMessage *internalClientMessage
}

func (s *Socket) SendData(data []byte, addr *net.UDPAddr, shouldACK bool) {

	b64 := base64.StdEncoding.EncodeToString(data)

	packet := internalPacket{
		ShouldRecvACK:      shouldACK,
		SequenceNumber:     s.nextSequenceNum,
		LastRequiredSeqNum: s.lastRequiredSeqNum,
		Payload:            b64,
	}

	if packet.SequenceNumber > s.lastRequiredSeqNum && shouldACK == true {
		s.lastRequiredSeqNum = packet.SequenceNumber
	}

	CM := internalClientMessage{
		Client: addr,
		Packet: &packet,
	}

	s.send <- &CM

	if packet.ShouldRecvACK {
		toBeACK := toBeAcknowledged{
			Deadline:      time.Now().Add(100 * time.Millisecond),
			ClientMessage: &CM,
		}

		s.toBeAck.mu.Lock()
		s.toBeAck.dict[CM.Packet.SequenceNumber] = &toBeACK
		s.toBeAck.mu.Unlock()
	}
	s.nextSequenceNum++
}

func (s *Socket) Dispose() {
	for i := 0; i < 4; i++ {
		s.close <- true
	}

}

func CreateSocket(addr *net.UDPAddr) (socket *Socket) {
	var sock Socket
	conn, err := net.ListenUDP("udp", addr)
	if err != nil {
		panic(err.(any))
	}
	sock.Addr = addr
	sock.conn = conn
	sock.Recv = make(chan *ExternalClientMessage, 500)
	sock.send = make(chan *internalClientMessage, 0)
	sock.toBeAck = struct {
		mu   sync.Mutex
		dict map[int]*toBeAcknowledged
	}{dict: make(map[int]*toBeAcknowledged, 100)}
	sock.close = make(chan bool, 0)
	sock.recvQ = struct {
		mu   sync.Mutex
		data []*internalClientMessage
	}{data: make([]*internalClientMessage, 0)}
	sock.backLog = make([]*internalClientMessage, 0)
	sock.nextSequenceNum = 1

	fmt.Printf("server listening on : %s\n", sock.Addr.String())
	//reads the udp socket and stores packets in recvQ after ordering them
	go sock.readUDP()

	//reads from RecvQ into Recv channel for the end user
	go sock.readFromRecvQ()

	//sends packets to clients
	go sock.sendUDP()

	//checks the toBeAck map for unacknowledged packets and sends them again
	go sock.checkACK()

	return &sock

}

func (s *Socket) evaluatePacket(CM *internalClientMessage) {

	packet := CM.Packet

	s.ackPacket(CM)

	if packet.SequenceNumber == s.lastRecvPacket+1 {
		s.recvQ.mu.Lock()
		s.recvQ.data = append(s.recvQ.data, CM)
		s.recvQ.mu.Unlock()
		s.lastRecvPacket = packet.SequenceNumber

		if packet.ShouldRecvACK {
			s.lastACKpacket = packet.SequenceNumber
		}

	} else if packet.SequenceNumber > s.lastRecvPacket+1 {

		if packet.LastRequiredSeqNum == s.lastACKpacket {
			s.recvQ.mu.Lock()
			s.recvQ.data = append(s.recvQ.data, CM)
			s.recvQ.mu.Unlock()
			s.lastRecvPacket = packet.SequenceNumber

			if packet.ShouldRecvACK {
				s.lastACKpacket = packet.SequenceNumber
			}

		} else {
			s.backLog = append(s.backLog, CM)

		}

	} else if packet.SequenceNumber < s.lastRecvPacket+1 {

	}
}

func (s *Socket) ackPacket(CM *internalClientMessage) {
	packet := CM.Packet

	if packet.ShouldRecvACK {

		ack := ACK{SequenceNumber: packet.SequenceNumber}
		payload, _ := json.Marshal(ack)

		packet := &internalPacket{
			Type:               "ACK",
			LastRequiredSeqNum: -1,
			Payload:            base64.StdEncoding.EncodeToString(payload),
		}

		cm := internalClientMessage{
			Client: CM.Client,
			Packet: packet,
		}

		s.send <- &cm
	}
}

func (s *Socket) readFromRecvQ() {
	ticker := time.NewTicker(10 * time.Millisecond)
	for {
		select {
		case <-ticker.C:
			s.recvQ.mu.Lock()
			if len(s.recvQ.data) == 0 {
				s.recvQ.mu.Unlock()
				continue
			}
			newSlice := make([]*internalClientMessage, len(s.recvQ.data))
			copy(newSlice, s.recvQ.data)
			s.recvQ.data = make([]*internalClientMessage, 0)
			s.recvQ.mu.Unlock()

			for _, v := range newSlice {

				data, err := base64.StdEncoding.DecodeString(v.Packet.Payload)
				if err != nil {
					panic(err)
				}

				toUser := ExternalClientMessage{
					Addr: v.Client,
					Data: data,
				}

				s.Recv <- &toUser
			}
		}
	}
}

func (s *Socket) readUDP() {
	ticker := time.NewTicker(10 * time.Millisecond)
	for {
		select {
		case <-ticker.C:
			length := len(s.backLog)
			for i := 0; i < length; i++ {
				cm := s.backLog[0]
				s.backLog = RemoveAt(s.backLog, 0)
				s.evaluatePacket(cm)
			}
			data := make([]byte, 1300)
			s.conn.SetReadDeadline(time.Now().Add(5 * time.Millisecond))
			n, addr, err := s.conn.ReadFromUDP(data)
			if err != nil {
				if err.Error() == fmt.Sprintf("read udp %s: i/o timeout", s.Addr.String()) {
					continue
				}
				panic(err.(any))
			}
			if n > 0 {
				reader := bytes.NewReader(data)
				data1 := make([]byte, n)
				_, err = io.ReadAtLeast(reader, data1, n)
				if err != nil {
					panic(err.(any))
				}

				var packet internalPacket
				err := json.Unmarshal(data1, &packet)
				if err != nil {
					panic(err)
				}

				if packet.Type == "ACK" {

					var ack ACK
					err := json.Unmarshal([]byte(packet.Payload), &ack)
					if err != nil {
						panic(err)
					}

					fmt.Printf("%#v\n", ack)

					s.toBeAck.mu.Lock()
					if _, ok := s.toBeAck.dict[ack.SequenceNumber]; ok {
						delete(s.toBeAck.dict, ack.SequenceNumber)
					}
					s.toBeAck.mu.Unlock()

					continue
				}

				CM := internalClientMessage{
					Client: addr,
					Packet: &packet,
				}
				s.evaluatePacket(&CM)

			}

		case <-s.close:
			return
		}
	}
}

func (s *Socket) sendUDP() {
	for {
		select {
		case clientMessage := <-s.send:
			data, _ := json.Marshal(clientMessage.Packet)
			_, err := s.conn.WriteToUDP(data, clientMessage.Client)
			if err != nil {
				panic(err.(any))
			}
		case <-s.close:
			return

		}
	}
}

func (s *Socket) checkACK() {
	ticker := time.NewTicker(10 * time.Millisecond)
	for {
		select {
		case <-ticker.C:
			s.toBeAck.mu.Lock()
			for i := range s.toBeAck.dict {
				if s.toBeAck.dict[i].Deadline.Before(time.Now()) {
					s.toBeAck.dict[i].Deadline = time.Now().Add(100 * time.Millisecond)

					fmt.Printf("resending: %d\n", s.toBeAck.dict[i].ClientMessage.Packet.SequenceNumber)

					s.send <- s.toBeAck.dict[i].ClientMessage
				}
			}
			s.toBeAck.mu.Unlock()
		}
	}
}
