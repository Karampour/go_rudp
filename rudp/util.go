package rudp

func RemoveAt[T any](slice []*T, i int) []*T {

	if len(slice) == 1 {
		slice[0] = nil
		slice = slice[:0]
		return slice
	}

	copy(slice[i:], slice[i+1:])
	slice[len(slice)-1] = nil
	slice = slice[:len(slice)-1]
	return slice
}
