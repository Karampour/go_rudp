package main

import (
	"encoding/json"
	"fmt"
	"net"
	"rudp/rudp"
	"time"
)

var Tick = 1
var Clients map[*net.UDPAddr]*net.UDPAddr

func main() {
	Clients = make(map[*net.UDPAddr]*net.UDPAddr, 10)

	addr, err := net.ResolveUDPAddr("udp", "192.168.1.13:8080")
	if err != nil {
		panic(err)
	}
	sock := rudp.CreateSocket(addr)

	go func() {
		for {
			select {
			case ECM := <-sock.Recv:
				fmt.Printf("Address:%s - Message:%s\n", ECM.Addr.String(), string(ECM.Data))
				_, ok := Clients[ECM.Addr]
				if !ok {
					Clients[ECM.Addr] = ECM.Addr
				}
			}
		}
	}()

	go func() {
		ticker := time.NewTicker(200 * time.Millisecond)
		for {
			if len(Clients) == 0 {
				continue
			}
			fmt.Println(Tick)
			select {
			case <-ticker.C:
				for _, v := range Clients {

					t, _ := json.Marshal(Tick)

					sock.SendData(t, v, true)
					Tick++
				}
			}
		}
	}()

	c := make(chan bool, 0)

	<-c
}
